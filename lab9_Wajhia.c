#include<stdio.h>
#include<stdlib.h>
typedef (*vtable)(struct Matrix *m);

vtable *t_matrix;
vtable *t_vector;
typedef struct Matrix {
int *array;
int column;
int row;
vtable *pointer_m;
void (*Matrix)( struct Matrix *mat, int column, int row);
void (*mult)( struct Matrix *mat, struct Matrix *matrix1, struct Matrix *matrix2);

void (*add)( struct Matrix *mat, struct Matrix *matrix1, struct Matrix *matrix2);

}Matrix;


typedef struct Vector{
Matrix matrix;

void (*vec)( struct vector *mat, int column, int row);
vtable *pointer_v;
}vector;

int L1_norm_m(Matrix *matrix1){
int a,b,sum,result=0;
for(a=0; a<matrix1->column; a++){
for(b=0; b<(matrix1->row); b++){
sum=sum+matrix1->array[a*matrix1->column +b];
}
if(sum>result){
result=sum;
}
}

return result;

}

int L1_norm_v(Matrix *m1){
int i,sum=0;

for(i=0; i<(m1->row); i++){
sum=sum+m1->array[i];

}

return sum;

}
void Add(Matrix *m1, Matrix *m2 , Matrix *result)
{
int *arr;
  

result->array=(int *)malloc( (m1->row*m1->column)* sizeof(int));
int i;
for(i=0;i<(m1->column*m1->row); i++){
	result->array[i]=m1->array[i]+m2->array[i];


}

}



void Multiply(Matrix *m1, Matrix *m2, Matrix *result){
int i,j,k,l,f,sum=0;

int size=m1->row*m2->column;
int size1=m1->row*m1->column;
int size3=m2->row*m2->column;

result->array=(int *)malloc( (size)* sizeof(int));
for ( i = 0; i<size1; i = i + m1->column){
j = i;
f = 0;
for (l = 0; l<size3;){
	sum = sum + (m1->array[j] * m2->array[l]);
 	l = l + m2->column;
	j++;
	if ((j) % m1->column == 0){
	result->array[k] = sum;
	j = i;
	if (l != (size3-1) + m2->column){
	f++;
	l = f;
}
k++;
sum=0;
			}
		}
	
}
}
void Matrix_init( Matrix *mat, int col, int row){
int **arr;
mat->add=Add;
mat->mult=Multiply;
mat->column=col;
mat->row= row;
int size=row*col;
mat->array=(int *)malloc(sizeof(int)*(size));
int i,j,k=0;


for(i=0;i<size; i++){
mat->array[i] = k;
k++;
}



}
void Vector_Add(vector *m1, vector *m2 , vector *result)
{
int *arr;
 
int size=m1->matrix.row*m1->matrix.column;
int size1=m1->matrix.column*m1->matrix.row;
result->matrix.array=(int *)malloc( (size)* sizeof(int));
int i;
for(i=0;i<size1; i++){
	result->matrix.array[i]=m1->matrix.array[i]+m2->matrix.array[i];


}
}
void Vector_init( vector *mat, int col, int row){
int **arr;
mat->matrix.add=Vector_Add;
mat->matrix.mult=Multiply;
mat->matrix.column=col;
mat->matrix.row= row;
int size=row*col;
mat->matrix.array=(int *)malloc(sizeof(int)*(size));
int i,k=0;


for(i=0;i<size; i++){
mat->matrix.array[i] = k;
k++;

}




}

int main(){
int row_1 , col_1 ,row_2 ,col_2 ;
int m;	
Matrix m1;
Matrix m2;
Matrix result;
Matrix result1;
vector vec1;vector vec2;vector vec3;
int i,j;
printf("Press 1 for Matrix functions, Press 2 for Vector functions:");
scanf("%d", &m);
if(m==1){

printf("Enter rows of Matrix 1:\n");
scanf("%d", &row_1);
printf("Enter columns of Matrix 1:\n");
scanf("%d", &col_1);
printf("Enter rows of Matrix 2:\n");
scanf("%d", &row_2);
printf("Enter columns of Matrix 2:\n");
scanf("%d", &col_2);

m1.Matrix=Matrix_init;
m1.Matrix(&m1, row_1,col_1);
m2.Matrix=Matrix_init;
m2.Matrix(&m2, row_2,col_2);
if(col_1==row_2){
m2.mult(&m1,&m2, &result1);
printf("MAtrix Multiplication:\n");
for(i=0; i<row_1;i++){
for (j=0; j<col_2; j++){
printf("%d  ", result1.array[i*m2.column +j]);
}
printf("\n");
}
}
else
printf("Multiplication is not possible\n");

	if (col_1 == col_2 && row_1==row_2){

m2.add(&m1, &m2,&result);
printf("Matrix Addition:\n");
for(i=0; i<m2.row;i++){
for (j=0; j<m2.column; j++){
printf("%d  ", result.array[i*m2.column +j]);
}
printf("\n");
}
}
else
printf("Addition is not possible.Run again\n");




t_matrix=(vtable *)malloc( (10)* sizeof(vtable));
t_matrix[0]=L1_norm_m;
m1.pointer_m=t_matrix;
m2.pointer_m=t_matrix;
printf("\n");
printf("%d :L1 norm of Matrix 1 \n",m1.pointer_m[0](&m1));
printf("%d :L1 norm of Matrix 2\n",m2.pointer_m[0](&m2));

}
if(m==2){
printf("Enter rows of Vector 1:\n");
scanf("%d", &row_2);
printf("Enter rows of Vector 2:\n");
scanf("%d", &col_2);
t_vector=(vtable *)malloc( (10)* sizeof(vtable));
vec2.vec=Vector_init;
vec2.vec(&vec2, 1, row_2);
vec1.vec=Vector_init;
vec1.vec(&vec1, 1, col_2);
if(row_2==col_2){
vec1.matrix.add(&vec1, &vec2, &vec3);
printf("Addition of vector:\n");
for(i=0; i<vec2.matrix.row;i++){

printf("%d  ", vec3.matrix.array[i]);

printf("\n");
}
}
else
printf("Addition is not possible\n");

t_vector[0]=L1_norm_v;
vec1.pointer_v=t_vector;
vec2.pointer_v=t_vector;
printf("\n");
printf("%d :L1 norm of Vector 1 \n",vec1.pointer_v[0](&vec1.matrix));
printf("%d :L1 norm of Vector 2 \n",vec2.pointer_v[0](&vec2.matrix));

}

}
